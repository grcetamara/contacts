import UIKit
//import Snap

class ViewController: UIViewController {

    // MARK: - Private Properites
    private var titleLabel: UILabel!
    private var userNameTextField: UITextField!
    private var passwordTextField: UITextField!
    private var logInButton: UIButton!
    private var questionLabel: UILabel!
    private var signUpButton: UIButton!
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    private func setUpViews() {
        titleLabel = UILabel()
        
        
        userNameTextField = UITextField()
        
        passwordTextField = UITextField()
        
        logInButton = UIButton()
        
        questionLabel = UILabel()
        
        signUpButton = UIButton()
    }
    
    func setUpConstraints() {
        self.view.addSubview(titleLabel)
        self.view.addSubview(userNameTextField)
        self.view.addSubview(passwordTextField)
        self.view.addSubview(logInButton)
        self.view.addSubview(questionLabel)
        self.view.addSubview(signUpButton)
        
        //titleLabel.snp
    }
    
}
